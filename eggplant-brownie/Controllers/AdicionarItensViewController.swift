//
//  AdicionarItensViewController.swift
//  eggplant-brownie
//
//  Created by Koda Gabriel on 14/11/20.
//  Copyright © 2020 com.koda. All rights reserved.
//

import UIKit

protocol AdicionaItensDelegate {
    func add(_ item: Item)
}

class AdicionarItensViewController: UIViewController {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var caloriasTextField: UITextField!
    
    // MARK: - Atributos
    var delegate: AdicionaItensDelegate?
    
    
    init(delegate: AdicionaItensDelegate) {
        super.init(nibName: "AdicionarItensViewController", bundle: nil)
        self.delegate = delegate
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


        // MARK: - IBActions
    
    
    @IBAction func adicionarItem(_ sender: Any) {
        guard let nome = nomeTextField?.text, let caloriasString = caloriasTextField?.text, let calorias = Double(caloriasString) else {return}
        let item = Item(nome: nome , calorias: calorias)
        delegate?.add(item)
        navigationController?.popViewController(animated: true)
        
    }
    
}
