//
//  RemoveRefeicaoViewController.swift
//  eggplant-brownie
//
//  Created by Koda Gabriel on 16/11/20.
//  Copyright © 2020 com.koda. All rights reserved.
//

import UIKit


class RemoveRefeicaoViewController {
    
    let controller: UIViewController
    
    
    init(controller: UIViewController) {
        self.controller = controller
    }
    
    func exibe(_ refeicao: Refeicao, handler: @escaping (UIAlertAction) -> Void) {
        let alerta = UIAlertController(title: refeicao.nome, message: refeicao.detalhes(), preferredStyle: .alert)
        
        let botaoCancelar = UIAlertAction(title: "Cancelar", style: .cancel)
        let botaoRemover = UIAlertAction(title: "Remover", style: .destructive, handler: handler)
        alerta.addAction(botaoCancelar)
        alerta.addAction(botaoRemover)
        controller.present(alerta, animated: true, completion: nil)
    }
}
