//
//  ViewController.swift
//  eggplant-brownie
//
//  Created by Koda Gabriel on 12/11/20.
//  Copyright © 2020 com.koda. All rights reserved.
//

import UIKit

protocol AdicionaRefeicaoDelegate {
    func add(_ refeicao: Refeicao)
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AdicionaItensDelegate{
    
    // MARK: - Atributos
    var delegate: AdicionaRefeicaoDelegate?
    
    var itens: [Item] = [
                            Item(nome: "Molho de tomate", calorias: 40.0),
                            Item(nome: "Queijo", calorias: 40.0),
                            Item(nome: "Manjericão", calorias: 40.0),
                            Item(nome: "Molho apimentado", calorias: 40.0),
                        ]
    
    var itensSelecionados: [Item] = []
    
    
    // MARK: - IBOutlet
    @IBOutlet var nomeTextField: UITextField?
    @IBOutlet var felicidadeTextField: UITextField?
    @IBOutlet weak var itensTableView: UITableView?
    
    // MARK: - View Life Cycles
    
    override func viewDidLoad() {
        let botaoAdicionaItem = UIBarButtonItem(title: "Adicionar", style: .plain, target: self, action: #selector(adicionarItem))
        navigationItem.rightBarButtonItem = botaoAdicionaItem
        itens = ItemDao().recupera()
        
    }
    
    // para funcionar também em OBJECTIVE-C
    @objc func adicionarItem() {
        let adicionarItensViewController = AdicionarItensViewController(delegate: self)
        navigationController?.pushViewController(adicionarItensViewController, animated: true)
    }
    
    
    func add(_ item: Item) {
        itens.append(item)
        
        if let tableView = itensTableView {
            tableView.reloadData()
        } else {
            Alerta(controller: self).exibe(message: "não foi possível atualizar a tabela")
        }
        ItemDao().salva(itens)
    }
    
    
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itens.count    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = UITableViewCell(style: .default, reuseIdentifier: nil)
        let linhaDaTabela = indexPath.row
        let item = itens[linhaDaTabela]
        celula.textLabel?.text = item.nome
        
        return celula
    }
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let celula = tableView.cellForRow(at: indexPath) else { return }
        
        if celula.accessoryType == .none {
            celula.accessoryType = .checkmark
            let linhaDaTabela = indexPath.row
            itensSelecionados.append(itens[linhaDaTabela])
        } else {
            let item = itens[indexPath.row]
            if let position = itensSelecionados.firstIndex(of: item) {
                itensSelecionados.remove(at: position)
            }
            celula.accessoryType = .none
        }
    }
    
    func recuperaRefeicaoDoFormulario() -> Refeicao? {
        guard let nome = nomeTextField?.text else {
            return nil
        }
        
        guard let felicidadeDaRefeicao = felicidadeTextField?.text, let felicidade = Int(felicidadeDaRefeicao) else {
            return nil
        }
        
        let refeicao = Refeicao(nome: nome, felicidade: felicidade, itens: itensSelecionados);
        
        return refeicao
    }
   
    // MARK: - IBActions
    @IBAction func adicionar(_ sender: Any) {
        
                guard let refeicao = recuperaRefeicaoDoFormulario() else {
                    Alerta(controller: self).exibe(message: "Erro ao ler dados do formulário")
                    return
                    }
                delegate?.add(refeicao)
                navigationController?.popViewController(animated: true)
            
        
    }
        
    
    
    

}

