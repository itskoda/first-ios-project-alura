//
//  ItemDao.swift
//  eggplant-brownie
//
//  Created by Koda Gabriel on 18/11/20.
//  Copyright © 2020 com.koda. All rights reserved.
//

import Foundation

class ItemDao {
    func salva(_ itens: [Item]) {
        guard let caminho = recuperaDiretorio() else {return}
        do {
            let dados = try NSKeyedArchiver.archivedData(withRootObject: itens, requiringSecureCoding: false)
            try dados.write(to: caminho)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func recupera() -> [Item] {
        guard let caminho = recuperaDiretorio() else { return []}
        
        do {
            let dados = try Data(contentsOf: caminho)
            let itensSalvos = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(dados) as! Array <Item>
            return itensSalvos
        } catch {
            print(error.localizedDescription)
            return []
        }
    }
    func recuperaDiretorio() -> URL? {
        guard let diretorio = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask).first else {return nil}
        let caminho = diretorio.appendingPathComponent("itens")
        
        return caminho
    }
}
