//
//  Alerta.swift
//  eggplant-brownie
//
//  Created by Koda Gabriel on 16/11/20.
//  Copyright © 2020 com.koda. All rights reserved.
//

import UIKit


class Alerta {
    
    let controller: UIViewController
    
    init (controller: UIViewController) {
        self.controller = controller
    }
    
    func exibe(title: String = "Desculpe", message: String) {
        let alerta = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alerta.addAction(ok)
        controller.present(alerta, animated: true)
    }
}
