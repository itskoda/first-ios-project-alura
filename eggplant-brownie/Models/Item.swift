//
//  Item.swift
//  eggplant-brownie
//
//  Created by Koda Gabriel on 14/11/20.
//  Copyright © 2020 com.koda. All rights reserved.
//

import UIKit

class Item: NSObject, NSCoding {
   
    // MARK: - Atributos
    
    let nome: String
    let calorias: Double
       
    
    // MARK: - INIT
    
    init (nome: String, calorias: Double) {
       self.nome  = nome
       self.calorias  = calorias
    }
    
    // MARK: - NSCoding
    
    func encode(with coder: NSCoder) {
        coder.encode(nome, forKey: "nome")
        coder.encode(calorias, forKey: "calorias")
       }
       
       required init?(coder: NSCoder) {
        nome = coder.decodeObject(forKey: "nome") as! String
        calorias = coder.decodeDouble(forKey: "calorias")
       }
}
